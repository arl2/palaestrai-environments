import pygame as pg

ROWS = 6
COLS = 7

CELL_SIZE = 80
SCORE1_X = 120
SCORE2_X = 600
SCORE_Y1 = 520
SCORE_Y2 = 540
BOARD_POS = (120, 30)
BOARD_COLOR = (80, 80, 80)
GREY = (100, 100, 100)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
ALPHA = (254, 254, 254)
WHITE = (255, 255, 255)
BUTTONS = {
    "1": [pg.K_1],
    "2": [pg.K_2],
    "3": [pg.K_3],
    "4": [pg.K_4],
    "5": [pg.K_5],
    "6": [pg.K_6],
    "7": [pg.K_7],
}
